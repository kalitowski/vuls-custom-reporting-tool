#!/usr/bin/env python3

# python3 modules

import json
import sys
import requests
import os
import re
import semantic_version
from jira import JIRA


# loading json file function

def load_json_file(file_name):
    with open(file_name) as json_file:
         data = json.load(json_file)
    return data

# getting list of report files from provided path

def get_report_files(path):
    list_of_files = {}
    for (dirpath, dirnames, filenames) in os.walk(path):
        for filename in filenames:
            if filename.endswith('.json'):
                list_of_files[filename] = os.sep.join([dirpath, filename])
    return list_of_files

# prepare report table with package cve information

def prepare_report(report_file, type):
    array=load_json_file('/usr/share/vuls-data/results/current/' + report_file)
    cves=list()
    report_table=list()
    report_table.append('||Package Name|'
                      + '|Fixed in|'
                      + '|Score|'
                      + '|URL||')

# get cve from report

    for cve in array['scannedCves']:
       cves.append(cve)

# iterate over cve list to get information about every single one

    for cve in cves:
       for package in array['scannedCves'][cve]['affectedPackages']:

        # there are two types of cve - fixed and not fixed - depends on your needs.

         if type not in package:
           continue
         if 'nvd' in array['scannedCves'][cve]['cveContents']:
           for i in array['scannedCves'][cve]['cveContents']['nvd']:
             cveScore=str(i['cvss2Score'])
         else:
           cveScore="N/A"
         report_table.append('{}{}{}{}'.format('|' + package['name'] + '|', package[type] + '|', '|'+ cveScore + '|', "https://nvd.nist.gov/vuln/detail/" + cve +'|'))
    return '\n'.join(report_table)

# this function get information about count of critical and non-critical cve which are not fixed

def check_cve(report_file, type):
    array=load_json_file('/usr/share/vuls-data/results/current/' + report_file)
    nonfixed_higher_seven_count=0
    nonfixed_lower_seven_count=0
    fixed_cves=list()
    fixed_cves.append("@here at **" + report[:-5] + "**\n")
    cves=list()
    for cve in array['scannedCves']:
       cves.append(cve)
    for cve in cves:
       for package in array['scannedCves'][cve]['affectedPackages']:
         if 'nvd' in array['scannedCves'][cve]['cveContents']: 
           if type not in package:
             for i in array['scannedCves'][cve]['cveContents']['nvd']:
               if i['cvss2Score'] > 7:
                 nonfixed_higher_seven_count=nonfixed_higher_seven_count+1
               else:
                 nonfixed_lower_seven_count=nonfixed_lower_seven_count+1
               continue

    reminder="There is **" + str(nonfixed_higher_seven_count) + "** CRITICAL and **" + str(nonfixed_lower_seven_count)  +"** NON-CRITICAL CVE on **" + report[:-5] + "**"
    return reminder

# preparing upgrade report of packages - it creates detailed table or simple one with only package count

def prepare_upgrade_report(report_file):
    array=load_json_file('/usr/share/vuls-data/results/current/' + report_file)
    report_table=list()
    update_count=0
    if simple_mode==False:
        report_table.append('|Package Name|'
                          + 'Current Version'
                          + '|New Version|')
        report_table.append('|---|---|---|')
    for package in array['packages']:
        if array['packages'][package]['newVersion'] != array['packages'][package]['version'] and array['packages'][package]['newVersion']:
            if simple_mode==False:
                report_table.append('{}{}{}'.format('|' + package + "|", array['packages'][package]['version'] + '|', array['packages'][package]['newVersion'] + '|'))
            else:
                update_count = update_count + 1 
    if simple_mode==True:
       report_table.append(str(update_count))

    if len(report_table) == 2:
      report_table = "*There is no updates*"
    else:
      report_table = '\n'.join(report_table)
    updates=check_software_from_git(report_file[:-5])
    if updates:
        if report_table is None:
            report_table = "No Apt Updates"
#        report_table = "**Update raport for: " + report_file[:-5] + "**\n\n" + report_table
        updates='\n'.join(updates)
        if simple_mode==False:
            report_table="Update raport for: " + "**"+report_file[:-5]+ "**" + "\n**Appications from github:**\n\n" + updates + "\nPackages in APT repositories\n\n" + report_table
        else:
            report_table="Update raport for: " + "**"+report_file[:-5]+ "**"+ "\n**Appications from github:**\n\n" + updates + "\nPackages count in APT repositories: " + report_table
        return report_table
    else:
        if simple_mode==False:
            report_table="Update raport for: " +"**"+ report_file[:-5]+ "**" + "\n**Packages in APT repositories**\n\n" + report_table
        else:
            report_table="Update raport for: " +"**"+ report_file[:-5]+ "**" + "\n**Packages count in APT repositories:** " + report_table
        return report_table

# it transforms provided version into form that is acceptable by semantic_version library

def transform_version(version):
    i=0
    formated_version=""
    for char in version:
        if char == "." and i < 3:
            i = i + 1
        if i <= 2 :
            formated_version = formated_version + char
    if re.match('(?<![\d.])(\d|\d*\.\d*)?(?![\d.])', formated_version) :
        formated_version = formated_version + ".0"
    return formated_version

# it removes unwanted characters from provided version

def remove_chars(version):
    if re.search('[a-zA-Z]', version):
        version = re.sub(r'[a-z]+', '', version, re.I)
        version = re.sub(r'[-]+', '', version, re.I)
    return version

# it checks which version is newer

def check_version(current_version, candidate_version):
    if semantic_version.validate(candidate_version):
        if semantic_version.Version(candidate_version) > semantic_version.Version(current_version):
            return candidate_version
        else:
            return current_version

# it generates report about software versions which are not installed from repository

def check_software_from_git(server):
    data = load_json_file("/home/sysadmin/vuls-custom-reporting-tool/software.json")
    software_list= list()
    software_list.append('|Name|'
               + 'Current Version'
               + '|New Version|')
    software_list.append('|---|---|---|')
    if server in data['servers']:
        for app in data['servers'][server]:
            r = requests.get(url=data['servers'][server][app]['giturl'], headers={'Content-Type': 'application/json'}).json()
            current_version = data['servers'][server][app]['version']
            for i in r:
                current_version = remove_chars(current_version)
                if "rc" in i['tag_name'] or "beta" in i['tag_name']:
                    i['tag_name']= '0.0.0'
                candidate_version = remove_chars(i['tag_name'])
                current_version = transform_version(current_version)
                candidate_version = transform_version(candidate_version)
                final_version = check_version(current_version, candidate_version)
                if (semantic_version.Version(final_version) > semantic_version.Version(current_version)):
                    current_version = final_version
            software_list.append('{}{}{}'.format("|"+app,"|"+ transform_version(data['servers'][server][app]['version']), "|"+ final_version +"|"))
        if len(software_list) > 2:
            return software_list
        else:
            return None
    else:
        return None

config=load_json_file("/home/sysadmin/vuls-custom-reporting-tool/config.json")

user = config['user']
apikey = config['apikey']
server = config['server_address']
webhook_url = config['webhook_url']
projects_list = config['projects']
projects_dict={}

# loading projects list into dictionary

for dict in projects_list:
    projects_dict =  {**projects_dict, **dict}

options = {
 'server': server
}

jira = JIRA(options, basic_auth=(user, apikey))

if __name__ == '__main__':
# checking arguments
    if len(sys.argv) == 2:
       if sys.argv[1] == '-s':
         simple_mode = True
       if sys.argv[1] == '-j':
         jira_mode = True
    else:
       simple_mode = False
    jira_mode = False
    reminders = list()
# reports files location
    report_files = get_report_files('/usr/share/vuls-data/results/current/')
# iterate over every report file
    for report in report_files:
# getting updates report from file
         updates = prepare_upgrade_report(report)
         for project in projects_dict.keys():
# checking if project name is in the report file name
             if project in report:
                 channel = projects_dict[project]
                 break
             else:
                 channel = projects_dict["bluebrick"]
# checking if updates variables is not None then send report to specific channel
         if updates:
              response = requests.post(
              webhook_url, json={"channel": channel,"text": updates },
              headers={'Content-Type': 'application/json'}
              )
# getting cves report
         reminder = check_cve(report,"fixedIn")
         reminders.append(reminder)
# checking if Jira mode is True if yes create/update tickets with CVE report
         if jira_mode == True:
             result = prepare_report(report,"fixedIn")
             if len(result) > 327677:
                 result = "ERROR - text is too long"
             if len(result) == 0:
                result = "No Fixed CVEs"
             issue_exist=jira.search_issues('summary ~ "CVEs for: %s"' %report[:-5])
             if issue_exist:
                issue = jira.issue(issue_exist[0].key)
                issue.update(
                    summary="CVEs for: %s" %report[:-5],
                    description="%s" %result
                )
             else:
                 jira.create_issue(project='OP',
                                   summary="CVEs for: %s" %report[:-5],
                                   description="%s" %result,
                                   issuetype={'name': 'CVE'})
# prepare report and send do channel
    reminders = '\n'.join(reminders)
    response = requests.post(
    webhook_url, json={"channel": 'cve-alerts',"text": "NotFixed CVE Report:\n\n" + reminders},
    headers={'Content-Type': 'application/json'}
    )
